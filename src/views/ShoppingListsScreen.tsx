import { View, StyleSheet } from 'react-native'
import React from 'react'
import { useTypedSelector } from '../hooks/useTypedSelector'
import { ShoppingListItem } from '../components/ShoppingListItem'

export const ShoppingListsScreen = () => {
  const state = useTypedSelector(state => state.shoppingLists)

  return (
    <View style={styles.container}>
      {state.shoppingLists.map(list => (
        <ShoppingListItem
          title={list.shoppingListTitle}
          products={list.products}
          key={list.id}/>)
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  }
})