import { StyleSheet } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { ShoppingListsScreen } from './ShoppingListsScreen'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { Feather } from '@expo/vector-icons';

const Tab = createBottomTabNavigator()

export const Navigator = () => {
  return (
    <NavigationContainer>
        <Tab.Navigator>
          <Tab.Screen
            name='Shopping Lists'
            component={ShoppingListsScreen}
            options={{
                tabBarIcon: () => <Feather name="list" size={24} color="black" />
            }}
          />

        </Tab.Navigator>
      </NavigationContainer>
  )
}


const styles = StyleSheet.create({})