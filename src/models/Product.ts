export interface Product {
    id: string
    productName: string
    productAmount: number | string
    productBought: boolean
}