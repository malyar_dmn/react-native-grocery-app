import { Product } from "./Product";

export interface ShoppingList {
    id: string
    shoppingListTitle: string
    products: Product[]
}