import { StyleSheet, Text, View } from 'react-native'
import React, { FC } from 'react'
import { Product } from '../models/Product'

interface Props {
    title: string
    products: Product[]
}

export const ShoppingListItem: FC<Props> = (props) => {
    console.log(props.products)

    return (
        <View style={styles.wrapper}>
            <Text>{props.title}</Text>
        </View>
    )
}


const styles = StyleSheet.create({
    wrapper: {
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 5,
        padding: 10
    }
})