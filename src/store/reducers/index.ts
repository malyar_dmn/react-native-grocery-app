import {combineReducers} from "redux";
import { shoppingListsReducer } from "./shoppingList/shoppingListsReducer";

export const rootReducer = combineReducers({
    shoppingLists: shoppingListsReducer
})

export type RootState = ReturnType<typeof rootReducer>