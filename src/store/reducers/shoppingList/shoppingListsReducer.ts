import { ShoppingListsAction, ShoppingListsActionTypes, ShoppingListsState } from "./types"
import 'react-native-get-random-values'
import {v4} from 'uuid'

const initialState: ShoppingListsState = {
    shoppingLists: [
        {
            id: v4(),
            shoppingListTitle: 'My list',
            products: [
                {id: v4(), productAmount: 1, productBought: false, productName: 'milk'}
            ]
        },
        {
            id: v4(),
            shoppingListTitle: 'Dinner', products: [
                {id: v4(), productAmount: 1, productBought: false, productName: 'milk'},
                {id: v4(), productAmount: 2, productBought: false, productName: 'bread'}
            ]
        }
    ]
}

export const shoppingListsReducer = (state = initialState, action: ShoppingListsAction): ShoppingListsState => {
    switch (action.type) {
        case ShoppingListsActionTypes.LOAD_SHOPPING_LIST:
            return {shoppingLists: action.payload}

        default:
            return state
    }
}