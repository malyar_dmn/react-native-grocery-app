import { ShoppingList } from "../../../models/ShoppingList";

export interface ShoppingListsState {
    shoppingLists: ShoppingList[]
}

export enum ShoppingListsActionTypes {
    LOAD_SHOPPING_LIST = 'LOAD_SHOPPING_LIST'
}

export interface ShoppingListsAction {
    type: string
    payload?: any
}